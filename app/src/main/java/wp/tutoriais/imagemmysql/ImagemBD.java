package wp.tutoriais.imagemmysql;

public class ImagemBD {
    public int id;
    public String imagemTexto;

    public ImagemBD(int id, String imagemTexto) {
        this.id = id;
        this.imagemTexto = imagemTexto;
    }
}
