package wp.tutoriais.imagemmysql;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TelaListarImagens extends AppCompatActivity {
    private List<ImagemBD> listaImagens = new ArrayList<>();
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_listar_imagens);

        //Criar objeto do RecyclerView
        recyclerView = findViewById(R.id.rvListaImagens);

        buscarTodasImagens();

    }

    //Método para criar uma requisição ao webservice e recuperar todas as imagens cadastradas
    private void buscarTodasImagens(){
        //Criar um objeto da classe Volley para configurar as requisições ao webservice
        RequestQueue solicitacao = Volley.newRequestQueue(this);
        //Configuração do endpoint (url) da requisição
        String url = "http://10.0.2.2:5000/api/Imagem/buscarTodos";

        //Configurando a requisição a ser enviada
        JsonArrayRequest envio = new JsonArrayRequest(Request.Method.GET,
            url, null,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    //Laço de repetição para percorrer cada objeto da lista
                    for(int i=0 ; i<response.length() ; i++){
                        try {
                            //Recuperar cada objeto do webservice
                            JSONObject object = response.getJSONObject(i);
                            //Criar um objeto da classe ImagemBD
                            ImagemBD imagemBD = new ImagemBD(object.getInt("id"), object.getString("imagemTexto"));
                            //Adicionar a imagem na lista
                            listaImagens.add(imagemBD);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        AdapterImagem adapterImagem = new AdapterImagem(listaImagens);
                        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
                        recyclerView.setAdapter(adapterImagem);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Toast.makeText(TelaListarImagens.this, "Erro ao conectar", Toast.LENGTH_SHORT).show();
                }
            }
        );

        solicitacao.add(envio);
    }
}