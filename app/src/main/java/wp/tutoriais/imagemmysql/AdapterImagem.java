package wp.tutoriais.imagemmysql;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterImagem extends RecyclerView.Adapter<ViewHolderImagem>{
    private List<ImagemBD> listaImagem;

    public AdapterImagem(List<ImagemBD> listaImagem){
        this.listaImagem = listaImagem;
    }

    @NonNull
    @Override
    public ViewHolderImagem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item, parent, false);
        return new ViewHolderImagem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderImagem holder, int position) {
        ViewHolderImagem vhImg = (ViewHolderImagem) holder;

        byte[] converteBase64 = Base64.decode(listaImagem.get(position).imagemTexto, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(converteBase64, 0, converteBase64.length);

        vhImg.imageViewItem.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        return listaImagem.size();
    }
}

class ViewHolderImagem extends RecyclerView.ViewHolder{
    ImageView imageViewItem;

    public ViewHolderImagem(@NonNull View itemView) {
        super(itemView);
        imageViewItem = itemView.findViewById(R.id.item_imagem);
    }
}